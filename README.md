> **`WARNING: incompatible with macOS High Sierra or higher`**

SuperVP-TRaX features the interactive design of sound transformations using either real time sound input or sound files loaded into the application. Through the use of an intuitive interface, the effect of all parameter modifications applied to sound transformations can be heard in real time.

![](https://forum.ircam.fr/media/uploads/Softwares/SuperVP%20Trax/visuel-supervptrax_01.jpg)


## Fields of Application ##

* Universal: high quality transposition and striking timbral changes.
* Educational: become familiar with the spectral envelope and cross-synthesis.
* Composition: intuitive and creative work on the voice, communication with SuperVP.
* Film & Video: transformation of speaker characteristics (gender/age).
* Post-Production: precise transposition and timbral adjustment.
* Sound Design: create original transformations of the voice.
* Amateur: create original samples for your favorite sequencer.

## Main Features ##

SuperVP TRaX is a tool designed for voice and music sonic transformations allowing independent transposition of pitch and timbre (spectral envelope). It offers precise control over all transformations (transposition, transposition jitter, component remixing, filtering, multiple delay lines and generalized cross synthesis). It features a creative set of presets. All parameter settings can be saved and recalled.

**Voice Transformations**

For single voice sounds there exist presets that allow for high quality transformations of the gender and age of the speaker. These presets can be fine tuned to the specific characteristics of the input voice.

**Musical Sound Transformations**

For musical sounds an additional mode for transient detection and preservation is available. When transient detection is enabled, the component remixing object allows for the independent remixing of the sinusoid, noise and transient components.

Transformations can be stored either as user defined presets or as SuperVP command lines. Using command lines enables the possibility to apply batch mode transformation to many sound files at once using the settings that have been designed with the SuperVP-TRaX tool.

![](https://forum.ircam.fr/media/uploads/Softwares/SuperVP%20Trax/supervptrax2.png)

> - [Sound Analysis-Synthesis](https://www.ircam.fr/recherche/equipes-recherche/anasyn/) Team.
> - [Processing by Phase Vocoder](https://www.ircam.fr/projects/pages/traitement-par-vocodeur-de-phase/)
>
> **Demonstrations**
>
> - [Voice transformation | multimedia application ](http://anasynth.ircam.fr/home/english/media/voice-transformation-multimedia-application). 
> Four characters voices were produced from the one of one actor by transformation of gender and age thanks to Super-VP and VoiceTrans 
> - [Analyse-Synthese team on TV: Voice transformation](http://anasynth.ircam.fr/home/english/media/analyse-synthese-team-tv-voice-transformation). 
> TV presentation of Analyse-Synthèse's SuperVP-TRaX software 
> - [Flux IRCAM Tools TRAX](http://anasynth.ircam.fr/home/english/media/flux-ircam-tools-trax). 
> Video of IRCAM Tools by Flux Spat from Musikmesse 2010 in Frankfurt
> - [Voice-transformation Examples](http://anasynth.ircam.fr/home/english/media/voice-transformation-examples). 
> Presented at Paris Game Developers Conference 2008
> - [Speech Research at Ircam demonstrated at Collège de France](http://anasynth.ircam.fr/home/english/media/speech-research-ircam-demonstrated-collège-de-france-2009). 
> Video of a conference 


